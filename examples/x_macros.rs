// This example is based on https://en.wikipedia.org/wiki/X_macro.

use mini_macro_magic::export;

export!(
    #[export(list_of_var$)]
    {[a, b, c]}
);

pub fn foo() {
    macro_rules! print {
        ({[$($v:ident),*]}) => {
            $(println!(stringify!($v));)*
        }
    }
    
    list_of_var!(print!());
}

pub fn bar() {
    macro_rules! print_with_bar {
        ({[$($v:ident),*]}) => {
            $(println!(concat!("BAR: ", stringify!($v)));)*
        }
    }
    
    list_of_var!(print_with_bar!());
}

fn main() {
    foo();
    bar();
}
