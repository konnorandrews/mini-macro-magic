#![allow(unused)]

use macro_rules_attribute::derive;
use mini_macro_magic::{emit, export};

#[derive(export!)]
#[custom(export(a$))]
struct A;

mod b {
    use super::derive;
    use super::*;

    // This one doesn't work at the crate root.

    #[derive(export!)]
    #[custom(export(pub b$))]
    pub struct B;
}

#[derive(export!)]
#[custom(export(pub(crate) c$))]
struct C;

#[derive(export!)]
#[custom(export(pub(self) d$))]
struct D;

#[derive(export!)]
#[custom(export(pub crate e$))]
struct E;

export!(
    #[export(f$)]
    {
        struct F;
    }
);
f!(emit!());

export!(
    #[export(pub(crate) g$)]
    {
        struct G;
    }
);
g!(emit!());

export!(
    #[export(pub(self) h$)]
    {
        struct H;
    }
);
h!(emit!());

export!(
    #[export(pub crate i$)]
    {
        struct I;
    }
);
i!(emit!());

mod j {
    use super::*;

    export!(
        #[export(pub j$)]
        {
            struct J;
        }
    );
}

// Emit the one defined in module j here.
j::j!(emit!());

// Using macro_rules_attribute's derive auto emits the definitions.
// When using export directly we need to invoke emit to emit the definition.
type _All = (A, b::B, C, D, E, F, G, H, I, J);
