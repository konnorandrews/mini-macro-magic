use macro_rules_attribute::derive;
use mini_macro_magic::export;

// An example struct definition.
#[derive(export!)]
#[custom(export(pub(crate) the_struct$))]
pub struct TheStruct(pub i32);

mod other {
    // We can create the same TheStruct here with emit.
    // Note this isn't the same type as the super::TheStruct it
    // just has the same definition.
    crate::the_struct!(mini_macro_magic::emit!());
}

mod other_other {
    // We can create the same TheStruct here with emit.
    crate::the_struct!(mini_macro_magic::emit!());

    pub mod inner {
        // We can create the same TheStruct here with emit.
        crate::the_struct!(mini_macro_magic::emit!());
    }
}

#[test]
fn all_the_structs_are_emitted() {
    assert_eq!(TheStruct(42).0, 42);
    assert_eq!(other::TheStruct(42).0, 42);
    assert_eq!(other_other::TheStruct(42).0, 42);
    assert_eq!(other_other::inner::TheStruct(42).0, 42);
}
