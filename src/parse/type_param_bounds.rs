#[macro_export]
macro_rules! parse_type_param_bounds {
    {
        {
            @trait_bound
            {$($callback:tt)*}
            {$($state:tt)*}

            [$($lifetimes:tt)*]
            [$($traits:tt)*]
        }

        {$($bound:tt)*}

        $($tail:tt)*
    } => {
        $crate::parse_type_param_bounds! {
            @{$($callback)*}
            {$($state)*}

            [$($lifetimes)*]
            [$($traits)* {$($bound)*}]

            $($tail)*
        }
    };
    {
        {$($callback:tt)*}
        {$($state:tt)*}

        $lt:lifetime

        $($tail:tt)*
    } => {
        $crate::parse_type_param_bounds! {
            @{$($callback)*}
            {$($state)*}

            [$lt]
            []

            $($tail)*
        }
    };
    {
        {$($callback:tt)*}
        {$($state:tt)*}

        $($tail:tt)*
    } => {
        $crate::parse_trait_bound! {
            {$crate::parse_type_param_bounds!}
            {
                @trait_bound
                {$($callback)*}
                {$($state)*}

                []
                []
            }

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        
        [$($lifetimes:tt)*]
        [$($traits:tt)*]

        + $lt:lifetime

        $($tail:tt)*
    } => {
        $crate::parse_type_param_bounds! {
            @{$($callback)*}
            {$($state)*}

            [$($lifetimes)* $lt]
            [$($traits)*]

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        
        [$($lifetimes:tt)*]
        [$($traits:tt)*]

        +

        $($tail:tt)*
    } => {
        $crate::parse_trait_bound! {
            {$crate::parse_type_param_bounds!}
            {
                @trait_bound
                {$($callback)*}
                {$($state)*}

                [$($lifetimes)*]
                [$($traits)*]
            }

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        
        [$($lifetimes:tt)*]
        [$($traits:tt)*]

        +

        $($tail:tt)*
    } => {
        $($callback)* {
            {$($state)*}
            
            {
                [$($lifetimes)*]
                [$($traits)*]
            }

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        
        [$($lifetimes:tt)*]
        [$($traits:tt)*]

        $($tail:tt)*
    } => {
        $($callback)* {
            {$($state)*}
            
            {
                [$($lifetimes)*]
                [$($traits)*]
            }

            $($tail)*
        }
    };
}
pub use parse_type_param_bounds;

#[cfg(test)]
mod test {
    use crate::parse::assert_tokens;

    use super::*;
    use quote::quote;

    #[test]
    fn lifetime() {
        let t = parse_type_param_bounds! {
            {quote!}
            {}

            'a

            extra
        };

        assert_tokens! {
            t
            {} 
            { 
                ['a] 
                [] 
            } 
            extra
        };
    }

    #[test]
    fn multiple_lifetime() {
        let t = parse_type_param_bounds! {
            {quote!}
            {}

            'a + 'b + 'c

            extra
        };

        assert_tokens! {
            t
            {} 
            { 
                ['a 'b 'c] 
                [] 
            } 
            extra
        };
    }

    #[test]
    fn r#trait() {
        let t = parse_type_param_bounds! {
            {quote!}
            {}

            Demo

            extra
        };

        assert_tokens! {
            t
            {} 
            { 
                [] 
                [{ Demo }] 
            } 
            extra
        };
    }

    #[test]
    fn multiple_trait() {
        let t = parse_type_param_bounds! {
            {quote!}
            {}

            Demo + Other<'a> + End

            extra
        };

        assert_tokens! {
            t
            {} 
            { 
                [] 
                [{ Demo } { Other::<'a,> } { End }] 
            } 
            extra
        };
    }

    #[test]
    fn mixed() {
        let t = parse_type_param_bounds! {
            {quote!}
            {}

            'a + Demo<'b> + for<'c> Other<'c>

            extra
        };

        assert_tokens! {
            t
            {} 
            { 
                ['a] 
                [
                    { Demo::<'b,> } 
                    { for<'c,> Other::<'c,> } 
                ] 
            } 
            extra
        };
    }
}
