#[macro_export]
macro_rules! parse_generic_args {
    {
        {
            @type
            {$($callback:tt)*}
            {$($state:tt)*}
            {
                [$($lifetimes:tt)*]
                [$($types:tt)*]
                [$($consts:tt)*]
                [$($bindings:tt)*]
            }
        }
        {$($parsed:tt)*}

        $($tail:tt)*
    } => {
        $crate::parse_generic_args! {
            @{$($callback)*}
            {$($state)*}
            {
                [$($lifetimes)*]
                [$($types)* {$($parsed)*}]
                [$($consts)*]
                [$($bindings)*]
            }

            $($tail)*
        }
    };
    {
        {$($callback:tt)*}
        {$($state:tt)*}

        <

        $($tail:tt)*
    } => {
        $crate::parse_generic_args! {
            @{$($callback)*}
            {$($state)*}
            {[] [] [] []}

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        {
            [$($lifetimes:tt)*]
            [$($types:tt)*]
            [$($consts:tt)*]
            [$($bindings:tt)*]
        }

        $lt:lifetime

        $($tail:tt)*
    } => {
        $crate::parse_generic_args! {
            @{$($callback)*}
            {$($state)*}
            {
                [$($lifetimes)* $lt]
                [$($types)*]
                [$($consts)*]
                [$($bindings)*]
            }

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        {
            [$($lifetimes:tt)*]
            [$($types:tt)*]
            [$($consts:tt)*]
            [$($bindings:tt)*]
        }

        ,

        $($tail:tt)*
    } => {
        $crate::parse_generic_args! {
            @{$($callback)*}
            {$($state)*}
            {
                [$($lifetimes)*]
                [$($types)*]
                [$($consts)*]
                [$($bindings)*]
            }

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        {[] [] [] []}

        >

        $($tail:tt)*
    } => {
        $($callback)* {
            {$($state)*}

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        {$($parsed:tt)*}

        >

        $($tail:tt)*
    } => {
        $($callback)* {
            {$($state)*}
            {$($parsed)*}

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        {
            [$($lifetimes:tt)*]
            [$($types:tt)*]
            [$($consts:tt)*]
            [$($bindings:tt)*]
        }

        $($tail:tt)*
    } => {
        $crate::parse_type! {
            {$crate::parse_generic_args!}
            {
                @type
                {$($callback)*}
                {$($state)*}
                {
                    [$($lifetimes)*]
                    [$($types)*]
                    [$($consts)*]
                    [$($bindings)*]
                }
            }

            $($tail)*
        }
    };
}
pub use parse_generic_args;

#[cfg(test)]
mod test {
    use crate::parse::assert_tokens;

    use super::*;
    use quote::quote;

    #[test]
    fn empty() {
        let t = parse_generic_args! {
            {quote!}
            {}

            <>

            extra
        };

        assert_tokens! {
            t
            {}
            extra
        }
    }

    #[test]
    fn lifetime() {
        let t = parse_generic_args! {
            {quote!}
            {}

            <'a>

            extra
        };

        assert_tokens! {
            t
            {} 
            { 
                ['a] 
                [] 
                [] 
                [] 
            } 
            extra
        };
    }

    #[test]
    fn multiple_lifetimes() {
        let t = parse_generic_args! {
            {quote!}
            {}

            <'a, 'b, 'c>

            extra
        };

        assert_tokens! {
            t
            {} 
            { 
                ['a 'b 'c] 
                [] 
                [] 
                [] 
            } 
            extra
        };
    }

    #[test]
    fn basic_type() {
        let t = parse_generic_args! {
            {quote!}
            {}
    
            <A>
    
            extra
        };

        assert_tokens! {
            t
            {} 
            { 
                [] 
                [{ A }] 
                [] 
                [] 
            } 
            extra
        };
    }
}
