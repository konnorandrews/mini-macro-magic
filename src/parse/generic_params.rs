#[macro_export]
macro_rules! parse_generic_params {
    {
        {$($callback:tt)*}
        {$($state:tt)*}

        <

        $($tail:tt)*
    } => {
        $crate::parse_generic_params! {
            @{$($callback)*}
            {$($state)*}
            {[] [] []}

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        {
            [$($lifetimes:tt)*]
            [$($types:tt)*]
            [$($consts:tt)*]
        }

        $(#[$($attr:tt)*])*
        $lt:lifetime: $bound:lifetime +

        $($tail:tt)*
    } => {
        $crate::parse_generic_params! {
            @lifetime
            {$($callback)*}
            {$($state)*}
            {
                [$($lifetimes)*]
                [$($types)*]
                [$($consts)*]
            }
            { $lt [$([$($attr)*])*] [$bound] }

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        {
            [$($lifetimes:tt)*]
            [$($types:tt)*]
            [$($consts:tt)*]
        }

        $(#[$($attr:tt)*])*
        $lt:lifetime: $bound:lifetime

        $($tail:tt)*
    } => {
        $crate::parse_generic_params! {
            @{$($callback)*}
            {$($state)*}
            {
                [$($lifetimes)* { $lt [$([$($attr)*])*] [$bound] }]
                [$($types)*]
                [$($consts)*]
            }

            $($tail)*
        }
    };
    {
        @lifetime
        {$($callback:tt)*}
        {$($state:tt)*}
        {
            [$($lifetimes:tt)*]
            [$($types:tt)*]
            [$($consts:tt)*]
        }
        { $lt:lifetime [$([$($attr:tt)*])*] [$($bounds:tt)*] }

        $bound:lifetime +

        $($tail:tt)*
    } => {
        $crate::parse_generic_params! {
            @lifetime
            {$($callback)*}
            {$($state)*}
            {
                [$($lifetimes)*]
                [$($types)*]
                [$($consts)*]
            }
            { $lt [$([$($attr)*])*] [$($bounds)* $bound] }

            $($tail)*
        }
    };
    {
        @lifetime
        {$($callback:tt)*}
        {$($state:tt)*}
        {
            [$($lifetimes:tt)*]
            [$($types:tt)*]
            [$($consts:tt)*]
        }
        { $lt:lifetime [$([$($attr:tt)*])*] [$($bounds:tt)*] }

        $bound:lifetime

        $($tail:tt)*
    } => {
        $crate::parse_generic_params! {
            @{$($callback)*}
            {$($state)*}
            {
                [$($lifetimes)* { $lt [$([$($attr)*])*] [$($bounds)* $bound] }]
                [$($types)*]
                [$($consts)*]
            }

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        {
            [$($lifetimes:tt)*]
            [$($types:tt)*]
            [$($consts:tt)*]
        }

        $(#[$($attr:tt)*])*
        $lt:lifetime

        $($tail:tt)*
    } => {
        $crate::parse_generic_params! {
            @{$($callback)*}
            {$($state)*}
            {
                [$($lifetimes)* { $lt [$([$($attr)*])*] }]
                [$($types)*]
                [$($consts)*]
            }

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        {
            [$($lifetimes:tt)*]
            [$($types:tt)*]
            [$($consts:tt)*]
        }

        $(#[$($attr:tt)*])*
        $type:ident:

        $($tail:tt)*
    } => {
        $crate::parse_type_param_bounds! {
            {$crate::parse_generic_params!}
            {
                @type_param_bounds
                {$($callback)*}
                {$($state)*}
                {
                    [$($lifetimes)*]
                    [$($types)*]
                    [$($consts)*]
                }
                { $type [$([$($attr)*])*] }
            }

            $($tail)*
        }
    };
    {
        {
            @type_param_bounds
            {$($callback:tt)*}
            {$($state:tt)*}
            {
                [$($lifetimes:tt)*]
                [$($types:tt)*]
                [$($consts:tt)*]
            }
            { $type:ident [$([$($attr:tt)*])*] }
        }
        {
            [$($lt:lifetime),*]
            [$({$($ty:tt)*})*]
        }

        $($tail:tt)*
    } => {
        $crate::parse_generic_params! {
            @{$($callback)*}
            {$($state)*}
            {
                [$($lifetimes)*]
                [$($types)* { $type [$([$($attr)*])*] [$($lt),*] [$({$($ty)*})*] }]
                [$($consts)*]
            }

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        {
            [$($lifetimes:tt)*]
            [$($types:tt)*]
            [$($consts:tt)*]
        }

        $(#[$($attr:tt)*])*
        $type:ident

        $($tail:tt)*
    } => {
        $crate::parse_generic_params! {
            @{$($callback)*}
            {$($state)*}
            {
                [$($lifetimes)*]
                [$($types)* { $type [$([$($attr)*])*] }]
                [$($consts)*]
            }

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        {
            [$($lifetimes:tt)*]
            [$($types:tt)*]
            [$($consts:tt)*]
        }

        ,

        $($tail:tt)*
    } => {
        $crate::parse_generic_params! {
            @{$($callback)*}
            {$($state)*}
            {
                [$($lifetimes)*]
                [$($types)*]
                [$($consts)*]
            }

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        {[] [] []}

        >

        $($tail:tt)*
    } => {
        $($callback)* {
            {$($state)*}

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        {$($parsed:tt)*}

        >

        $($tail:tt)*
    } => {
        $($callback)* {
            {$($state)*}
            {$($parsed)*}

            $($tail)*
        }
    };
}
pub use parse_generic_params;

#[cfg(test)]
mod test {
    use crate::parse::assert_tokens;
    use quote::quote;
    use super::*;

    #[test]
    fn empty() {
        let t = parse_generic_params! {
            {quote!}
            {}

            <>

            extra
        };

        assert_tokens! {
            t
            {} 
            extra
        };
    }

    #[test]
    fn lifetime() {
        let t = parse_generic_params! {
            {quote!}
            {}

            <#[something] 'a>

            extra
        };

        assert_tokens! {
            t
            {} 
            { 
                [{ 'a [[something]] }] 
                [] 
                [] 
            } 
            extra
        };
    }

    #[test]
    fn multiple_lifetimes() {
        let t = parse_generic_params! {
            {quote!}
            {}

            <#[something] 'a, 'b: 'a, 'c: 'a + 'b + 'd, 'd>

            extra
        };

        assert_tokens! {
            t 
            {} 
            { 
                [
                    { 'a [[something]] } 
                    { 'b [] ['a] } 
                    { 'c [] ['a 'b 'd] } 
                    { 'd [] }
                ] 
                [] 
                [] 
            }
            extra
        };
    }

    #[test]
    fn r#type() {
        let t = parse_generic_params! {
            {quote!}
            {}

            <#[something] T>

            extra
        };

        assert_tokens! {
            t
            {} 
            { 
                [] 
                [{ T [[something]] }] 
                [] 
            } 
            extra
        };
    }

    #[test]
    fn multiple_type() {
        trace_macros!(true);
        let t = parse_generic_params! {
            {quote!}
            {}
    
            <A, #[b] B: Demo + Borrow<str> + 'a, C>
    
            extra
        };
        trace_macros!(false);
    
        assert_tokens! {
            t
            {} 
            { 
                [] 
                [
                    { A [] } 
                    { B [[b]] ['a] [{ Demo } { Borrow::<str,> }] }
                    { C [] }
                ]
                [] 
            } 
            extra
        };
    }
}
