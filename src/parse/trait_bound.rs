#[macro_export]
macro_rules! parse_trait_bound {
    {
        {$($callback:tt)*}
        {$($state:tt)*}

        ($($inner:tt)*)

        $($tail:tt)*
    } => {
        $($callback)* {
            {$($state)*}
            {($($inner)*)}

            $($tail)*
        }
    };
    {
        {$($callback:tt)*}
        {$($state:tt)*}

        for

        $($tail:tt)*
    } => {
        $crate::parse_generic_params! {
            {$crate::parse_trait_bound!}
            {
                @for
                {$($callback)*}
                {$($state)*}
            }

            $($tail)*
        }
    };
    {
        {
            @for
            {$($callback:tt)*}
            {$($state:tt)*}
        }
        {$($parsed:tt)*}

        $($tail:tt)*
    } => {
        $crate::parse_type_path! {
            {$crate::parse_trait_bound!}
            {
                @for_type_path
                {$($callback)*}
                {$($state)*}
                {$($parsed)*}
            }

            $($tail)*
        }
    };
    {
        {
            @for_type_path
            {$($callback:tt)*}
            {$($state:tt)*}
            {
                [$({
                    $lt:lifetime
                    [$($lt_attr:tt)*]
                })*]
                []
                []
            }
        }
        {$($parsed:tt)*}

        $($tail:tt)*
    } => {
        $($callback)* {
            {$($state)*}
            {for<$($($lt_attr)* $lt,)*> $($parsed)*}

            $($tail)*
        }
    };
    {
        {$($callback:tt)*}
        {$($state:tt)*}

        ?

        $($tail:tt)*
    } => {
        $crate::parse_type_path! {
            {$crate::parse_trait_bound!}
            {
                @maybe
                {$($callback)*}
                {$($state)*}
            }

            $($tail)*
        }
    };
    {
        {
            @maybe
            {$($callback:tt)*}
            {$($state:tt)*}
        }
        {$($parsed:tt)*}

        $($tail:tt)*
    } => {
        $($callback)* {
            {$($state)*}
            {?$($parsed)*}

            $($tail)*
        }
    };
    {
        {$($callback:tt)*}
        {$($state:tt)*}

        $($tail:tt)*
    } => {
        $crate::parse_type_path! {
            {$($callback)*}
            {$($state)*}

            $($tail)*
        }
    };
}
pub use parse_trait_bound;

#[cfg(test)]
mod test {
    use crate::parse::assert_tokens;

    use super::*;
    use quote::quote;

    #[test]
    fn type_path() {
        let t = parse_trait_bound! {
            {quote!}
            {}

            Demo

            extra
        };

        assert_tokens! {
            t
            {} 
            { Demo } 
            extra
        };
    }

    // #[test]
    // fn for_lt_type_path() {
    //     let s = parse_trait_bound! {
    //         {stringify!}
    //         {}
    //
    //         for<'a> Demo<'a>
    //
    //         extra
    //     };
    //     assert_eq!(s, "{} { for<'a> Demo<'a> } extra");
    // }

    #[test]
    fn maybe_type_path() {
        let t = parse_trait_bound! {
            {quote!}
            {}

            ?Sized

            extra
        };

        assert_tokens! {
            t
            {} 
            { ?Sized }
            extra
        };
    }

    #[test]
    fn type_path_paren() {
        let t = parse_trait_bound! {
            {quote!}
            {}

            (Demo)

            extra
        };

        assert_tokens! {
            t
            {} 
            { (Demo) } 
            extra
        };
    }

    #[test]
    fn maybe_type_path_paren() {
        let t = parse_trait_bound! {
            {quote!}
            {}

            (?Sized)

            extra
        };

        assert_tokens! {
            t
            {} 
            { (?Sized) } 
            extra
        };
    }
}
