#[macro_export]
macro_rules! parse_type_path {
    {
        {
            @generic_args
            {$($callback:tt)*}
            {$($state:tt)*}
            {$($parsed:tt)*}
        }

        { 
            [$($lt:lifetime),*] 
            [$({$($ty:tt)*}),*] 
            [] 
            [] 
        }

        $($tail:tt)*
    } => {
        $crate::parse_type_path! {
            @{$($callback)*}
            {$($state)*}
            {$($parsed)* ::<$($lt,)* $($($ty)*,)*>}

            $($tail)*
        }
    };
    {
        {$($callback:tt)*}
        {$($state:tt)*}

        $segment:ident

        $($tail:tt)*
    } => {
        $crate::parse_type_path! {
            @{$($callback)*}
            {$($state)*}
            {$segment}

            $($tail)*
        }
    };
    {
        {$($callback:tt)*}
        {$($state:tt)*}

        ::$segment:ident

        $($tail:tt)*
    } => {
        $crate::parse_type_path! {
            @{$($callback)*}
            {$($state)*}
            {::$segment}

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        {$($parsed:tt)*}

        ::$segment:ident

        $($tail:tt)*
    } => {
        $crate::parse_type_path! {
            @{$($callback)*}
            {$($state)*}
            {$($parsed)*::$segment}

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        {$($parsed:tt)*}

        $(::)? <

        $($tail:tt)*
    } => {
        $crate::parse_generic_args! {
            {$crate::parse_type_path!}
            {
                @generic_args
                {$($callback)*}
                {$($state)*}
                {$($parsed)*}
            }

            <

            $($tail)*
        }
    };
    {
        @{$($callback:tt)*}
        {$($state:tt)*}
        {$($parsed:tt)*}

        $($tail:tt)*
    } => {
        $($callback)* {
            {$($state)*}
            {$($parsed)*}

            $($tail)*
        }
    };
}
pub use parse_type_path;

#[cfg(test)]
mod test {
    use crate::parse::assert_tokens;
    use quote::quote;

    use super::*;

    #[test]
    fn ident() {
        let t = parse_type_path! {
            {quote!}
            {}

            thing

            extra
        };

        assert_tokens! {
            t
            {} 
            { thing } 
            extra
        };
    }

    #[test]
    fn root_ident() {
        let t = parse_type_path! {
            {quote!}
            {}

            ::thing

            extra
        };

        assert_tokens! {
            t
            {} 
            { ::thing } 
            extra
        };
    }

    #[test]
    fn basic() {
        let t = parse_type_path! {
            {quote!}
            {}

            root::child::Inner

            extra
        };

        assert_tokens! {
            t
            {} 
            { root::child::Inner } 
            extra
        };
    }

    // #[test]
    // fn with_type_generic() {
    //     let s = parse_type_path! {
    //         {stringify!}
    //         {}
    //
    //         Borrow<str>
    //
    //         extra
    //     };
    //     assert_eq!(s, "{} { Borrow < str > } extra");
    // }
}
