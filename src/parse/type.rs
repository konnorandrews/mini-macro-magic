#[macro_export]
macro_rules! parse_type {
    {
        {$($callback:tt)*}
        {$($state:tt)*}

        ($type:ty)

        $($tail:tt)*
    } => {
        $($callback)* {
            {$($state)*}
            {($type)}

            $($tail)*
        }
    };
    {
        {$($callback:tt)*}
        {$($state:tt)*}

        impl

        $($tail:tt)*
    } => {
        $crate::parse_trait_bound! {
            {$($callback)*}
            {
                @impl
                {$($state)*}
            }

            $($tail)*
        }
    };
    {
        {$($callback:tt)*}
        {$($state:tt)*}

        $($tail:tt)*
    } => {
        $crate::parse_type_path! {
            {$($callback)*}
            {$($state)*}

            $($tail)*
        }
    };
}
pub use parse_type;

#[cfg(test)]
mod test {
    use crate::parse::assert_tokens;

    use super::*;
    use quote::quote;

    #[test]
    fn paren_type() {
        let t = parse_type! {
            {quote!}
            {}

            (i32)

            extra
        };

        assert_tokens! {
            t
            {} 
            { (i32) } 
            extra
        };
    }
}
