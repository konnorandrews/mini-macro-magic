mod type_path;
mod generic_args;
mod r#type;
mod trait_bound;
mod generic_params;
mod type_param_bounds;


macro_rules! assert_tokens {
    ($tokens:ident $($t:tt)*) => {
        pretty_assertions::assert_eq!($tokens.to_string(), quote::quote!($($t)*).to_string());
    }
}
pub(crate) use assert_tokens;
