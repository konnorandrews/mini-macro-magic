[self]: https://gitlab.com/konnorandrews/mini-macro-magic
[`export`]: https://docs.rs/mini-macro-magic/{{version}}/mini_macro_magic/macro.export.html

[![Version](https://img.shields.io/static/v1?label=version&message={{version}}&color=informational)]()
[![Crates.io](https://img.shields.io/crates/v/mini-macro-magic)](https://crates.io/crates/mini-macro-magic)
[![docs.rs](https://img.shields.io/docsrs/mini-macro-magic)](https://docs.rs/mini-macro-magic/{{version}}/mini_macro_magic/)
[![Crates.io](https://img.shields.io/crates/l/mini-macro-magic)](#license)

# `::{{crate}}` 🪄

{{readme}}

<br>

#### License

<sup>
Licensed under either of <a href="LICENSE-APACHE">Apache License, Version
2.0</a> or <a href="LICENSE-MIT">MIT license</a> at your option.
</sup>

<br>

<sub>
Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in mini-macro-rules by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
</sub>
